#version 330

in vec3 vViewSpacePosition;
in vec3 vViewSpaceNormal;
in vec2 vTexCoords;

uniform vec3 uLightingDirection;
uniform vec3 uLightingRadiance;

uniform sampler2D uBaseColorTexture;
uniform vec4 uBaseColorFactor;

uniform float uMetallicFactor;
uniform float uRoughnessFactor;
uniform sampler2D uMetallicRoughnessTexture;

out vec3 fColor;

// Constants
const float GAMMA = 2.2;
const float INV_GAMMA = 1. / GAMMA;
const float M_PI = 3.141592653589793;
const float M_1_PI = 1.0 / M_PI;
const vec3 dielectricSpecular = vec3(0.04, 0.04, 0.04);
const vec3 black = vec3(0, 0, 0);

// We need some simple tone mapping functions
// Basic gamma = 2.2 implementation
// Stolen here: https://github.com/KhronosGroup/glTF-Sample-Viewer/blob/master/src/shaders/tonemapping.glsl

// linear to sRGB approximation
// see http://chilliant.blogspot.com/2012/08/srgb-approximations-for-hlsl.html
vec3 LINEARtoSRGB(vec3 color) {
    return pow(color, vec3(INV_GAMMA));
}

// sRGB to linear approximation
// see http://chilliant.blogspot.com/2012/08/srgb-approximations-for-hlsl.html
vec4 SRGBtoLINEAR(vec4 srgbIn) {
    return vec4(pow(srgbIn.xyz, vec3(GAMMA)), srgbIn.w);
}

void main() {
    vec3 N = normalize(vViewSpaceNormal);
    vec3 L = uLightingDirection;
    vec3 V = normalize(-vViewSpacePosition);
    vec3 H = normalize(L + V);

    vec4 baseColorFromTexture = SRGBtoLINEAR(texture(uBaseColorTexture, vTexCoords));
    vec4 baseColor = baseColorFromTexture * uBaseColorFactor;

    vec4 metallicRougnessFromTexture = texture(uMetallicRoughnessTexture, vTexCoords);

    float metallic = metallicRougnessFromTexture.b * uMetallicFactor;
    float roughness = metallicRougnessFromTexture.g * uRoughnessFactor ;

    vec3 C_diff = mix(baseColor.rgb * (1 - dielectricSpecular.r), black, metallic);
    vec3 F0 = mix(dielectricSpecular, baseColor.rgb, metallic);
    float alpha = roughness * roughness;
    float NdotL = clamp(dot(N, L), 0, 1);

    float VdotH = clamp(dot(V,H), 0, 1);
    float NdotV = clamp(dot(N,V), 0, 1);
    float NdotH = clamp(dot(N,H), 0, 1);

    float alpha2 = alpha * alpha;

    vec3 F = F0 + (1-F0)*(1 - VdotH)*(1 - VdotH)*(1 - VdotH)*(1 - VdotH)*(1 - VdotH);
    float denom_Vis = ((NdotL) * sqrt(NdotV * NdotV * (1-alpha) * (1 - alpha) + alpha2) + NdotV * sqrt(NdotL * NdotL * (1-alpha) * (1 - alpha) + alpha2));

    float Vis = (denom_Vis > 0) ? 0.5/denom_Vis : 0;

    float D = alpha2 / (M_PI * (NdotH*NdotH * (alpha2 -1) +1) * (NdotH*NdotH * (alpha2 -1) +1) );
    vec3 diffuse = C_diff * M_1_PI;

    vec3 fDiffuse = (1-F) * diffuse;
    vec3 fSpecular = F * Vis * D;

    fColor = LINEARtoSRGB((fDiffuse + fSpecular) * uLightingRadiance * NdotL);

}
