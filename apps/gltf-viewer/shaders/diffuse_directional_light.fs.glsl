#version 330

in vec3 vViewSpacePosition;
in vec3 vViewSpaceNormal;
in vec2 vTexCoords;

out vec3 fColor;

uniform vec3 uLightingDirection;
uniform vec3 uLightingRadiance;

const float pi = 3.14;

vec3 bidirectionalReflectanceDistribution(vec3 LightingDirection, vec3 viewDirection){
   return vec3(1/pi);
}

void main()
{
   vec3 viewDirection = -vViewSpacePosition;
   fColor = bidirectionalReflectanceDistribution(uLightingDirection,viewDirection) * uLightingRadiance * dot(normalize(vViewSpaceNormal),uLightingDirection);
}