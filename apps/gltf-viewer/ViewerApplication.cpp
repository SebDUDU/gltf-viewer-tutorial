#include "ViewerApplication.hpp"

#include <iostream>
#include <numeric>

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/io.hpp>

#include "utils/cameras.hpp"
#include "utils/gltf.hpp"
#include "utils/images.hpp"
#include "utils/trackballCameraController.hpp"
#include "utils/firstPersonCameraController.hpp"

#include <stb_image_write.h>
#include <tiny_gltf.h>


void keyCallback(GLFWwindow *window, int key, int scancode, int action, int mods) {
    if (key == GLFW_KEY_ESCAPE && action == GLFW_RELEASE) {
        glfwSetWindowShouldClose(window, 1);
    }
}

int ViewerApplication::run() {
    if (!loadGltfFile()) {
        return EXIT_FAILURE;
    }

    // Loader shaders
    const auto glslProgram =
            compileProgram({m_ShadersRootPath / m_AppName / m_vertexShader,
                            m_ShadersRootPath / m_AppName / m_fragmentShader});

    Locations locations(glslProgram);

    glm::vec3 bboxMin;
    glm::vec3 bboxMax;
    computeSceneBounds(m_model, bboxMin, bboxMax);

    glm::vec3 diagonalVector = (bboxMax - bboxMin);

    // Build projection matrix
    float maxDistance = (bboxMax == bboxMin) ? 100.f : glm::l2Norm(diagonalVector);
    maxDistance = maxDistance > 0.f ? maxDistance : 100.f;
    const auto projMatrix =
            glm::perspective(70.f, float(m_nWindowWidth) / m_nWindowHeight,
                             0.001f * maxDistance, 2.f * maxDistance);

    m_cameraController->setSpeed(2.f * maxDistance);
    if (!m_hasUserCamera) {
        glm::vec3 center = 0.5f * (bboxMax + bboxMin);
        glm::vec3 up(0, 1, 0);

        glm::vec3 eye = (bboxMax.z == bboxMin.z) ? center + 2.f * glm::cross(diagonalVector, up) : bboxMax +
                                                                                                   diagonalVector;
        m_cameraController->setCamera(
                Camera{eye, center, up});
    }

    size_t texturesSize = m_model.textures.size() + 1;
    GLuint textures[texturesSize];
    glGenTextures(texturesSize, textures);
    const GLuint &whiteTexture = textures[texturesSize - 1];

    createTextureObjects(textures, texturesSize - 1, whiteTexture);


    size_t buffersSize = m_model.buffers.size();
    GLuint buffers[buffersSize];
    glGenBuffers(buffersSize, buffers);

    createBufferObjects(buffers, buffersSize);

    size_t vaosSize = 0;
    for (auto &mesh : m_model.meshes) {
        vaosSize += mesh.primitives.size();
    }

    GLuint vaos[vaosSize];

    glGenVertexArrays(vaosSize, vaos);

    createVertexArrayObjects(buffers, vaos);

    GLuint *primitivOffset[m_model.meshes.size()];
    unsigned int sum = 0;

    auto itVaos = vaos;
    auto firstVao = itVaos;

    primitivOffset[0] = firstVao;
    auto itPrim = primitivOffset + 1;

    for (auto it = m_model.meshes.cbegin(); it != m_model.meshes.cend() - 1; ++it) {
        sum += it->primitives.size();
        *itPrim = (firstVao + sum);
    }

    // Setup OpenGL state for rendering
    glEnable(GL_DEPTH_TEST);
    glslProgram.use();

    glm::vec3 lightDirection(1.f);
    glm::vec3 lightIntensity(1.f);
    bool lightFromCamera = false;

    if (!m_OutputPath.empty()) {
        std::vector<unsigned char> pixels(m_nWindowWidth * m_nWindowHeight * 3);
        renderToImage(m_nWindowWidth, m_nWindowHeight, 3, pixels.data(), [&]() {
            drawScene(lightIntensity, locations, projMatrix, primitivOffset, textures, whiteTexture);
        });

        flipImageYAxis(m_nWindowWidth, m_nWindowHeight, 3, pixels.data());

        const auto strPath = m_OutputPath.string();
        stbi_write_png(
                strPath.c_str(), m_nWindowWidth, m_nWindowHeight, 3, pixels.data(), 0);

        glDeleteBuffers(buffersSize, buffers);
        glDeleteVertexArrays(vaosSize, vaos);
        glDeleteTextures(texturesSize, textures);
        //TODO clean IBOs

        return EXIT_SUCCESS;
    }

    float color[3] = {1.f, 1.f, 1.f};
    float theta = 0.f;
    float phi = 0.f;
    float intensity = 1;


    // Loop until the user closes the window
    for (auto iterationCount = 0u; !m_GLFWHandle.shouldClose();
         ++iterationCount) {
        const auto seconds = glfwGetTime();
        const auto camera = m_cameraController->getCamera();
        drawScene(lightIntensity, locations, projMatrix, primitivOffset, textures, whiteTexture);

        // GUI code:
        imguiNewFrame();

        {
            ImGui::Begin("GUI");
            ImGui::Text("Application average %.3f ms/frame (%.1f FPS)",
                        1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
            if (ImGui::CollapsingHeader("Camera", ImGuiTreeNodeFlags_DefaultOpen)) {
                ImGui::Text("eye: %.3f %.3f %.3f", camera.eye().x, camera.eye().y,
                            camera.eye().z);
                ImGui::Text("center: %.3f %.3f %.3f", camera.center().x,
                            camera.center().y, camera.center().z);
                ImGui::Text(
                        "up: %.3f %.3f %.3f", camera.up().x, camera.up().y, camera.up().z);

                ImGui::Text("front: %.3f %.3f %.3f", camera.front().x, camera.front().y,
                            camera.front().z);
                ImGui::Text("left: %.3f %.3f %.3f", camera.left().x, camera.left().y,
                            camera.left().z);

                if (ImGui::Button("CLI camera args to clipboard")) {
                    std::stringstream ss;
                    ss << "--lookat " << camera.eye().x << "," << camera.eye().y << ","
                       << camera.eye().z << "," << camera.center().x << ","
                       << camera.center().y << "," << camera.center().z << ","
                       << camera.up().x << "," << camera.up().y << "," << camera.up().z;
                    const auto str = ss.str();
                    glfwSetClipboardString(m_GLFWHandle.window(), str.c_str());
                }

                static int cameraControllerType = 0;
                bool isTrackBallSelected = ImGui::RadioButton("Trackball", &cameraControllerType, 0);
                ImGui::SameLine(0.0f, 10.0f);
                bool isFPCSelected = ImGui::RadioButton("First Person", &cameraControllerType, 1);
                const auto currentCamera = m_cameraController->getCamera();

                if (isTrackBallSelected) {
                    m_cameraController = std::make_unique<TrackballCameraController>(
                            m_GLFWHandle.window(), 2.f * maxDistance);
                }
                if (isFPCSelected) {
                    m_cameraController = std::make_unique<FirstPersonCameraController>(
                            m_GLFWHandle.window(), 2.f * maxDistance);
                }

                m_cameraController->setCamera(currentCamera);
            }
            if (locations.lightingDirection >= 0) {

                if (ImGui::CollapsingHeader("Light control", ImGuiTreeNodeFlags_DefaultOpen)) {

                    ImGui::SliderFloat("light intensity", &intensity, 0, 10);

                    ImGui::ColorEdit3("light color", color);

                    lightIntensity = intensity * glm::vec3(color[0], color[1], color[2]);

                    ImGui::Checkbox("light from camera", &lightFromCamera);

                    if (lightFromCamera) {
                        glUniform3f(locations.lightingDirection, 0, 0, 1);
                    } else {

                        ImGui::SliderAngle("phi", &phi, 0, 360);
                        ImGui::SliderAngle("theta", &theta, 0, 180);

                        const auto viewMatrix = camera.getViewMatrix();

                        lightDirection = glm::vec3(sin(theta) * cos(phi), cos(theta), sin(theta) * sin(phi));

                        const auto dir = glm::normalize(glm::vec3(viewMatrix * glm::vec4(lightDirection, 0)));
                        glUniform3f(locations.lightingDirection, dir[0], dir[1], dir[2]);
                    }
                }

            }
            ImGui::End();
        }

        imguiRenderFrame();

        glfwPollEvents(); // Poll for and process events

        auto ellapsedTime = glfwGetTime() - seconds;
        auto guiHasFocus =
                ImGui::GetIO().WantCaptureMouse || ImGui::GetIO().WantCaptureKeyboard;
        if (!guiHasFocus) {
            m_cameraController->update(float(ellapsedTime));
        }

        m_GLFWHandle.swapBuffers(); // Swap front and back buffers
    }

    glDeleteBuffers(buffersSize, buffers);
    glDeleteVertexArrays(vaosSize, vaos);
    glDeleteTextures(texturesSize, textures);
    //TODO clean IBOs

    return EXIT_SUCCESS;
}

ViewerApplication::ViewerApplication(const fs::path &appPath, uint32_t width,
                                     uint32_t height, const fs::path &gltfFile,
                                     const std::vector<float> &lookatArgs, const std::string &vertexShader,
                                     const std::string &fragmentShader, const fs::path &output) :
        m_nWindowWidth(width),
        m_nWindowHeight(height),
        m_AppPath{appPath},
        m_AppName{m_AppPath.stem().string()},
        m_ImGuiIniFilename{m_AppName + ".imgui.ini"},
        m_ShadersRootPath{m_AppPath.parent_path() / "shaders"},
        m_gltfFilePath{gltfFile},
        m_OutputPath{output} {

    m_cameraController = std::make_unique<TrackballCameraController>(m_GLFWHandle.window());

    if (!lookatArgs.empty()) {
        m_hasUserCamera = true;
        m_cameraController->setCamera(Camera{glm::vec3(lookatArgs[0], lookatArgs[1], lookatArgs[2]),
                                             glm::vec3(lookatArgs[3], lookatArgs[4], lookatArgs[5]),
                                             glm::vec3(lookatArgs[6], lookatArgs[7], lookatArgs[8])});
    }

    if (!vertexShader.empty()) {
        m_vertexShader = vertexShader;
    }

    if (!fragmentShader.empty()) {
        m_fragmentShader = fragmentShader;
    }

    ImGui::GetIO().IniFilename =
            m_ImGuiIniFilename.c_str(); // At exit, ImGUI will store its windows
    // positions in this file

    glfwSetKeyCallback(m_GLFWHandle.window(), keyCallback);

    printGLVersion();
}

bool ViewerApplication::loadGltfFile() {
    tinygltf::TinyGLTF t;
    std::string err;
    std::string warn;
    if (t.LoadASCIIFromFile(&m_model, &err, &warn, m_gltfFilePath.string())) {
        return true;
    }

    std::cerr << "WARNING/ERROR WHEN LOADED GLTF FILE :" << std::endl
              << " -> name : " << m_gltfFilePath.string() << std::endl;

    if (!warn.empty()) {
        std::cout << " -> warning : " << warn << std::endl;
    }
    if (!err.empty()) {
        std::cout << " -> error : " << err << std::endl;
    }

    std::cout << std::endl;

    return false;
}

void ViewerApplication::createBufferObjects(const GLuint *buffers, size_t buffersSize) {

    auto itBuffers = m_model.buffers.cbegin();
    auto itIdxBuffer = buffers;

    for (int i = 0; i < buffersSize; ++i, ++itBuffers, ++itIdxBuffer) {
        GLuint idxBuffer = *itIdxBuffer;
        tinygltf::Buffer buffer = *itBuffers;

        glBindBuffer(GL_ARRAY_BUFFER, idxBuffer);

        glBufferData(GL_ARRAY_BUFFER, buffer.data.size(), buffer.data.data(), GL_STATIC_DRAW);
    }
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void ViewerApplication::createVertexArrayObjects(const GLuint *buffers, GLuint *vaos) {

    GLuint *itVaos = vaos;

    const tinygltf::Mesh *itMesh = m_model.meshes.data();
    const size_t meshesSize = m_model.meshes.size();

    for (int i = 0; i < meshesSize; ++i, ++itMesh) {
        auto &mesh = *itMesh;

        const tinygltf::Primitive *itPrim = mesh.primitives.data();
        const size_t primSize = mesh.primitives.size();


        for (int j = 0; j < primSize; ++j, ++itPrim, ++itVaos) {
            auto &primitive = *itPrim;
            auto &vao = *itVaos;

            glBindVertexArray(vao);

            GLsizei attributeSize = -1;

            for (auto &focusedAttribute : focusedAttributes) {

                auto accessorIdx = primitive.attributes.find(focusedAttribute.first);

                assert(accessorIdx != primitive.attributes.end());

                tinygltf::Accessor accessor = m_model.accessors[(*accessorIdx).second];
                tinygltf::BufferView bufferView = m_model.bufferViews[accessor.bufferView];

                assert(GL_ARRAY_BUFFER == bufferView.target);
                auto vbo = buffers[bufferView.buffer];

                glBindBuffer(GL_ARRAY_BUFFER, vbo);

                glEnableVertexAttribArray(focusedAttribute.second); /** meshes[i].primitives.attributes */

                glVertexAttribPointer(focusedAttribute.second,//type d'attribut /** meshes[i].primitives.attributes */
                                      accessor.type, //2composantes en 2D /** accessor[i].type */
                                      accessor.componentType, //type des composantes /** accessor[i].componentType*/
                                      GL_FALSE, //Normalized or not /** check for accessors[i].min and accessors[i].max */
                                      bufferView.byteStride, //taille entre 2 attributs /** bufferViews[i].byteStride */
                                      (const void *) (bufferView.byteOffset +
                                                      accessor.byteOffset)); //offset /** bufferViews[i].byteOffset + accessors[i].byteOffset */

                if (attributeSize == -1) {
                    attributeSize = static_cast<GLsizei>(accessor.count);
                }

                assert(attributeSize == static_cast<GLsizei>(accessor.count));
            }

            int idxIbo = primitive.indices;

            if (idxIbo >= 0) {
                const auto &accessor = m_model.accessors[idxIbo];
                const auto &bufferView = m_model.bufferViews[accessor.bufferView];
                const auto bufferIdx = bufferView.buffer;

                assert(GL_ELEMENT_ARRAY_BUFFER == bufferView.target);
                glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,
                             buffers[bufferIdx]);
            }
            glBindVertexArray(0);
        }
    }

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void ViewerApplication::createTextureObjects(const GLuint *textures, size_t texturesSize, const GLuint &whiteTexture) {
    tinygltf::Sampler defaultSampler;
    defaultSampler.minFilter = GL_LINEAR;
    defaultSampler.magFilter = GL_LINEAR;
    defaultSampler.wrapS = GL_REPEAT;
    defaultSampler.wrapT = GL_REPEAT;
    defaultSampler.wrapR = GL_REPEAT;

    const float white[] = {1, 1, 1, 1};

    glActiveTexture(GL_TEXTURE0);

    glBindTexture(GL_TEXTURE_2D, whiteTexture);

    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 1, 1, 0,
                 GL_RGBA, GL_FLOAT, white);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, defaultSampler.minFilter);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, defaultSampler.magFilter);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, defaultSampler.wrapS);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, defaultSampler.wrapT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_R, defaultSampler.wrapR);

    glBindTexture(GL_TEXTURE_2D, 0);

    auto idxTex = textures;
    auto itTex = m_model.textures.data();

    for (int i = 0; i < texturesSize; ++i, ++idxTex, ++itTex) {
        const GLuint &idTex = *idxTex;
        const tinygltf::Texture &texture = *itTex;

        glBindTexture(GL_TEXTURE_2D, idTex);

        assert(texture.source >= 0);
        const auto &image = m_model.images[texture.source];

        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, image.width, image.height, 0,
                     GL_RGBA, image.pixel_type, image.image.data());

        const auto &sampler = (texture.sampler < 0) ? defaultSampler : m_model.samplers[texture.sampler];

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
                        sampler.minFilter != -1 ? sampler.minFilter : defaultSampler.minFilter);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,
                        sampler.magFilter != -1 ? sampler.magFilter : defaultSampler.magFilter);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, sampler.wrapS);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, sampler.wrapT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_R, sampler.wrapR);

        if (sampler.minFilter == GL_NEAREST_MIPMAP_NEAREST ||
            sampler.minFilter == GL_NEAREST_MIPMAP_LINEAR ||
            sampler.minFilter == GL_LINEAR_MIPMAP_NEAREST ||
            sampler.minFilter == GL_LINEAR_MIPMAP_LINEAR) {
            glGenerateMipmap(GL_TEXTURE_2D);
        }
    }
    glBindTexture(GL_TEXTURE_2D, 0);
}


void ViewerApplication::drawScene(const glm::vec3 &lightIntensity, const Locations &locations,
                                  const glm::mat4 &projMatrix, GLuint **primitivOffset,
                                  const GLuint *textures, const GLuint &whiteTexture) {
    glViewport(0, 0, m_nWindowWidth, m_nWindowHeight);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    if (locations.lightingRadiance >= 0) {
//        std::cout << lightIntensity << std::endl;
        glUniform3fv(locations.lightingRadiance, 1, glm::value_ptr(lightIntensity));
    }

    // Draw the scene referenced by gltf file
    if (m_model.defaultScene >= 0) {
        for (auto &node : m_model.scenes[m_model.defaultScene].nodes) {
            const glm::mat4 viewMatrix = m_cameraController->getCamera().getViewMatrix();
            drawNode(node, viewMatrix, glm::mat4(1), locations, projMatrix, primitivOffset, textures, whiteTexture);
        }
    }
}

void ViewerApplication::drawNode(int nodeIdx, const glm::mat4 &viewMatrix, const glm::mat4 &parentMatrix,
                                 const Locations &locations, const glm::mat4 &projMatrix,
                                 GLuint **primitivOffset, const GLuint *textures, const GLuint &whiteTexture) {
    auto &node = m_model.nodes[nodeIdx];
    auto m_modelMatrix = getLocalToWorldMatrix(node, parentMatrix);

    if (node.mesh >= 0) {
        auto &mesh = m_model.meshes[node.mesh];

        auto MVMatrix = viewMatrix * m_modelMatrix;

        auto NormalMatrix = glm::transpose(glm::inverse(MVMatrix));

        glUniformMatrix4fv(locations.modelViewMatrix, 1, GL_FALSE, glm::value_ptr(MVMatrix));
        glUniformMatrix4fv(locations.normalMatrix, 1, GL_FALSE, glm::value_ptr(NormalMatrix));
        glUniformMatrix4fv(locations.modelViewProjMatrix, 1, GL_FALSE,
                           glm::value_ptr(projMatrix * MVMatrix));

        GLuint *itVaos = (*primitivOffset) + node.mesh;

        for (const auto &primitiv : mesh.primitives) {

            bindMaterial(primitiv.material, locations, textures, whiteTexture);

            glBindVertexArray(*itVaos);

            if (primitiv.indices >= 0) {
                //std::cout << " glDrawElements" << std::endl;
                auto &accessor = m_model.accessors[primitiv.indices];
                glDrawElements(primitiv.mode, accessor.count, accessor.componentType,
                               (const void *) (accessor.byteOffset +
                                               m_model.bufferViews[accessor.bufferView].byteOffset));
            } else {
                auto &accessor = m_model.accessors[primitiv.attributes.begin()->second];
                glDrawArrays(GL_TRIANGLES, 0, accessor.count); // pourquoi 0 ?
            }
            glBindVertexArray(0);
            ++itVaos;
        }
    }
    for (const auto &childrenId : node.children) {
        drawNode(childrenId, viewMatrix, m_modelMatrix, locations, projMatrix, primitivOffset, textures, whiteTexture);
    }
}

void ViewerApplication::bindMaterial(int materialIdx, const Locations &locations, const GLuint *textures,
                                     const GLuint &whiteTexture) {
    glActiveTexture(GL_TEXTURE0);

    if (materialIdx < 0) {
        glBindTexture(GL_TEXTURE_2D, whiteTexture);
        glUniform1i(locations.baseColorTexture, 0);

        glUniform4f(locations.baseColorFactor, 1, 1, 1, 1);
        return;
    }

    tinygltf::PbrMetallicRoughness &pbr = m_model.materials[materialIdx].pbrMetallicRoughness;
    const tinygltf::TextureInfo &baseColorTexture = pbr.baseColorTexture;
    if (baseColorTexture.index >= 0) {
        glBindTexture(GL_TEXTURE_2D, textures[baseColorTexture.index]);
        glUniform1i(locations.baseColorTexture, 0);
    } else {
        glBindTexture(GL_TEXTURE_2D, whiteTexture);
        glUniform1i(locations.baseColorTexture, 0);
    }

    glUniform4f(locations.baseColorFactor,
                (float) pbr.baseColorFactor[0],
                (float) pbr.baseColorFactor[1],
                (float) pbr.baseColorFactor[2],
                (float) pbr.baseColorFactor[3]);

    if(pbr.metallicRoughnessTexture.index > 0){
        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, textures[pbr.metallicRoughnessTexture.index]);
        glUniform1i(locations.metallicRoughnessTexture, 1);
    }

    glUniform1f(locations.metallicFactor, pbr.metallicFactor);

    glUniform1f(locations.roughnessFactor,pbr.roughnessFactor);

}

