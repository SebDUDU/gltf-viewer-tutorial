#pragma once

#include <tiny_gltf.h>
#include "utils/GLFWHandle.hpp"
#include "utils/cameras.hpp"
#include "utils/filesystem.hpp"
#include "utils/shaders.hpp"
#include "utils/CameraController.hpp"

struct Locations {
    const GLuint modelViewProjMatrix;
    const GLuint modelViewMatrix;
    const GLuint normalMatrix;
    const GLuint lightingDirection;
    const GLuint lightingRadiance;
    const GLuint baseColorTexture;
    const GLuint baseColorFactor;
    const GLuint metallicFactor;
    const GLuint roughnessFactor;
    const GLuint metallicRoughnessTexture;

    Locations(const GLProgram &glslProgram) :
            modelViewProjMatrix(
                    glGetUniformLocation(glslProgram.glId(), "uModelViewProjMatrix")),
            modelViewMatrix(
                    glGetUniformLocation(glslProgram.glId(), "uModelViewMatrix")),
            normalMatrix(
                    glGetUniformLocation(glslProgram.glId(), "uNormalMatrix")),
            lightingDirection(
                    glGetUniformLocation(glslProgram.glId(), "uLightingDirection")),
            lightingRadiance(
                    glGetUniformLocation(glslProgram.glId(), "uLightingRadiance")),
            baseColorTexture(
                    glGetUniformLocation(glslProgram.glId(), "uBaseColorTexture")),
            baseColorFactor(
                    glGetUniformLocation(glslProgram.glId(), "uBaseColorFactor")),
            metallicFactor(
                    glGetUniformLocation(glslProgram.glId(), "uMetallicFactor")),
            roughnessFactor(
                    glGetUniformLocation(glslProgram.glId(), "uRoughnessFactor")),
            metallicRoughnessTexture(
                    glGetUniformLocation(glslProgram.glId(), "uMetallicRoughnessTexture"))
                    {}
};

class ViewerApplication {
public:
    ViewerApplication(const fs::path &appPath, uint32_t width, uint32_t height,
                      const fs::path &gltfFile, const std::vector<float> &lookatArgs,
                      const std::string &vertexShader, const std::string &fragmentShader,
                      const fs::path &output);

    int run();

private:
    static constexpr GLuint VERTEX_ATTR_POSITION = 0;
    static constexpr GLuint VERTEX_ATTR_NORMAL = 1;
    static constexpr GLuint VERTEX_ATTR_TEXCOORD_0 = 2;

    static constexpr std::pair<const char *, GLuint> focusedAttributes[] = {
            std::make_pair("POSITION", VERTEX_ATTR_POSITION),
            std::make_pair("NORMAL", VERTEX_ATTR_NORMAL),
            std::make_pair("TEXCOORD_0", VERTEX_ATTR_TEXCOORD_0)};

    GLsizei m_nWindowWidth = 1280;
    GLsizei m_nWindowHeight = 720;

    const fs::path m_AppPath;
    const std::string m_AppName;
    const fs::path m_ShadersRootPath;

    fs::path m_gltfFilePath;
    std::string m_vertexShader = "forward.vs.glsl";
    std::string m_fragmentShader = "pbr_directional_light.fs.glsl";

    bool m_hasUserCamera = false;
    std::unique_ptr<CameraController> m_cameraController;

    fs::path m_OutputPath;
    tinygltf::Model m_model;

    // Order is important here, see comment below
    const std::string m_ImGuiIniFilename;
    // Last to be initialized, first to be destroyed:
    GLFWHandle m_GLFWHandle{int(m_nWindowWidth), int(m_nWindowHeight),
                            "glTF Viewer",
                            m_OutputPath.empty()}; // show the window only if m_OutputPath is empty

    bool loadGltfFile();

    void createBufferObjects(const GLuint *buffers, size_t buffersSize);
    void createVertexArrayObjects(const GLuint *buffers, GLuint *vaos);
    void createTextureObjects(const GLuint *textures, size_t texturesSize , const GLuint & white);

    void drawScene(const glm::vec3 &lightIntensity, const Locations &locations, const glm::mat4 &projMatrix,
                   GLuint **primitivOffset, const GLuint * textures, const GLuint & whiteTexture);

    void drawNode(int nodeIdx, const glm::mat4 &viewMatrix, const glm::mat4 &parentMatrix,
                  const Locations &locations, const glm::mat4 &projMatrix,
                  GLuint **primitivOffset, const GLuint * textures, const GLuint & whiteTexture);

    void bindMaterial(int materialIdx, const Locations & locations, const GLuint * textures, const GLuint & whiteTexture);

    /*
      ! THE ORDER OF DECLARATION OF MEMBER VARIABLES IS IMPORTANT !
      - m_ImGuiIniFilename.c_str() will be used by ImGUI in ImGui::Shutdown, which
      will be called in destructor of m_GLFWHandle. So we must declare
      m_ImGuiIniFilename before m_GLFWHandle so that m_ImGuiIniFilename
      destructor is called after.
      - m_GLFWHandle must be declared before the creation of any object managing
      OpenGL resources (e.g. GLProgram, GLShader) because it is responsible for
      the creation of a GLFW windows and thus a GL context which must exists
      before most of OpenGL function calls.
    */
};