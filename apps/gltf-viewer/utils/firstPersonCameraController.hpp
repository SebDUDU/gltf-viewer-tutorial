#pragma once

#include "CameraController.hpp"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

struct GLFWwindow;

class FirstPersonCameraController : public CameraController {
public:
    FirstPersonCameraController(GLFWwindow *window, float speed = 1.f,
                                const glm::vec3 &worldUpAxis = glm::vec3(0, 1, 0)) : CameraController(window, speed,
                                                                                                      worldUpAxis) {}


    // Update the view matrix based on input events and elapsed time
    // Return true if the view matrix has been modified
    bool update(float elapsedTime) override;

private:

    // Input event state
    bool m_LeftButtonPressed = false;
};