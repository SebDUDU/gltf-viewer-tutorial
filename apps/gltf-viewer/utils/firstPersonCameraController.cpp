#include "firstPersonCameraController.hpp"
#include "glfw.hpp"

bool FirstPersonCameraController::update(float elapsedTime) {
    auto speed = CameraController::getSpeed();
    GLFWwindow * window = getWindow();

    glm::dvec2 cursorDelta;
    glm::dvec2 lastCursorPosition = getLastCursorPosition();
    if (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT)){
        if(!m_LeftButtonPressed){
            m_LeftButtonPressed = true;
            glfwGetCursorPos(window, &lastCursorPosition.x, &lastCursorPosition.y);
        }
        glm::dvec2 cursorPosition;
        glfwGetCursorPos(window, &cursorPosition.x, &cursorPosition.y);
        cursorDelta = cursorPosition - lastCursorPosition;
        setLastCursorPosition(cursorPosition);
    } else {
        m_LeftButtonPressed = false;
        cursorDelta = glm::dvec2(0);
    }

    float truckLeft = 0.f;
    float pedestalUp = 0.f;
    float dollyIn = 0.f;
    float rollRightAngle = 0.f;

    if (glfwGetKey(window, GLFW_KEY_W)) {
        dollyIn += speed * elapsedTime;
    }

    // Truck left
    if (glfwGetKey(window, GLFW_KEY_A)) {
        truckLeft += speed * elapsedTime;
    }

    // Pedestal up
    if (glfwGetKey(window, GLFW_KEY_UP)) {
        pedestalUp += speed * elapsedTime;
    }

    // Dolly out
    if (glfwGetKey(window, GLFW_KEY_S)) {
        dollyIn -= speed * elapsedTime;
    }

    // Truck right
    if (glfwGetKey(window, GLFW_KEY_D)) {
        truckLeft -= speed * elapsedTime;
    }

    // Pedestal down
    if (glfwGetKey(window, GLFW_KEY_DOWN)) {
        pedestalUp -= speed * elapsedTime;
    }

    if (glfwGetKey(window, GLFW_KEY_Q)) {
        rollRightAngle -= 0.001f;
    }
    if (glfwGetKey(window, GLFW_KEY_E)) {
        rollRightAngle += 0.001f;
    }

    // cursor going right, so minus because we want pan left angle:
    const float panLeftAngle = -0.01f * float(cursorDelta.x);
    const float tiltDownAngle = 0.01f * float(cursorDelta.y);

    const auto hasMoved = truckLeft || pedestalUp || dollyIn || panLeftAngle ||
                          tiltDownAngle || rollRightAngle;
    if (!hasMoved) {
        return false;
    }

    auto camera = getCamera();

    camera.moveLocal(truckLeft, pedestalUp, dollyIn);
    camera.rotateLocal(rollRightAngle, tiltDownAngle, 0.f);
    camera.rotateWorld(panLeftAngle, getWorldUpAxis());

    setCamera(camera);
    return true;
}