#include "trackballCameraController.hpp"
#include "glfw.hpp"


bool TrackballCameraController::update(float elapsedTime) {
    auto speed = CameraController::getSpeed();
    GLFWwindow * window = getWindow();

    glm::dvec2 cursorDelta;
    glm::dvec2 lastCursorPosition = getLastCursorPosition();
    if (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_MIDDLE)) {
        if (!m_MiddleButtonPressed) {
            m_MiddleButtonPressed = true;
            glfwGetCursorPos(window, &lastCursorPosition.x, &lastCursorPosition.y);
        }
        glm::dvec2 cursorPosition;
        glfwGetCursorPos(window, &cursorPosition.x, &cursorPosition.y);
        cursorDelta = cursorPosition - lastCursorPosition;
        setLastCursorPosition(cursorPosition);
    } else {
        m_MiddleButtonPressed = false;
        return false;
    }

    float dolly = 0.f;
    float orthoMove = 0.f;
    auto camera = getCamera();

    if (glfwGetKey(window, GLFW_KEY_LEFT_SHIFT)) {
        orthoMove += speed * elapsedTime;
        camera.truckLeft(orthoMove * cursorDelta.x);
    }
    if (glfwGetKey(window, GLFW_KEY_LEFT_CONTROL)) { // Need to check that eye != center
        glm::vec3 translateToCenter = normalize(camera.center() - camera.eye());
        dolly += speed * elapsedTime;
        camera.setEye(camera.eye() + translateToCenter * dolly * float(cursorDelta.y));
    }
    if (!dolly && !orthoMove) { //90 deg pb
        glm::vec3 translateToCenter = camera.center() - camera.eye();
        auto t1 = glm::translate(glm::mat4(1), translateToCenter);
        auto r1 = glm::rotate(t1, float(speed * elapsedTime * cursorDelta.x), camera.up());
        auto r2 = glm::rotate(r1, float(speed * elapsedTime * cursorDelta.y), camera.left());
        auto t2 = glm::translate(r2, -translateToCenter);

        glm::vec4 newPos = glm::vec4(camera.eye(), 1) * t2;

        camera.setEye(newPos);
    }

    setCamera(camera);

    return true;
}