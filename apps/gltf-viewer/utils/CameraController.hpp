#pragma once

#include <glm/vec3.hpp>
#include <glm/common.hpp>
#include "cameras.hpp"

struct GLFWwindow;

class CameraController {
public:
    CameraController(GLFWwindow *window,
                     float speed,
                     const glm::vec3 &worldUpAxis) :
            m_pWindow(window),
            m_fSpeed(speed),
            m_worldUpAxis(worldUpAxis),
            m_camera{glm::vec3(0), glm::vec3(0, 0, -1), glm::vec3(0, 1, 0)},
            m_LastCursorPosition{0, 0} {}

    // Update the view matrix based on input events and elapsed time
    // Return true if the view matrix has been modified
    virtual bool update(float elapsedTime) = 0;

    // Controller attributes, if put in a GUI, should be adapted
    void setSpeed(float speed) { m_fSpeed = speed; }

    float getSpeed() const { return m_fSpeed; }

    void increaseSpeed(float delta) {
        m_fSpeed += delta;
        m_fSpeed = glm::max(m_fSpeed, 0.f);
    }

    const glm::vec3 &getWorldUpAxis() const { return m_worldUpAxis; }

    void setWorldUpAxis(const glm::vec3 &worldUpAxis) {
        m_worldUpAxis = worldUpAxis;
    }

    // Get the view matrix
    const Camera &getCamera() const { return m_camera; }

    void setCamera(const Camera &camera) { m_camera = camera; }

    const glm::dvec2 &getLastCursorPosition() const {
        return m_LastCursorPosition;
    }

    void setLastCursorPosition(const glm::dvec2 &mLastCursorPosition) {
        m_LastCursorPosition = mLastCursorPosition;
    }

    GLFWwindow *&getWindow() {
        return m_pWindow;
    }

private:
    float m_fSpeed;
    glm::vec3 m_worldUpAxis;

    // Current camera
    Camera m_camera;

    GLFWwindow *m_pWindow;
    glm::dvec2 m_LastCursorPosition;
};